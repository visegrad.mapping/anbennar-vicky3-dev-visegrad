﻿namespace = mheromar_events

# The Malachite Knight Arrives
mheromar_events.1 = {
	type = country_event
	placement = root

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/bicorne_hat.dds"

	title = mheromar_events.1.t
	desc = mheromar_events.1.d
	flavor = mheromar_events.1.f

	cooldown = { months = short_modifier_time }

	duration = 3

	dlc = dlc004

	trigger = {
		has_dlc_feature = voice_of_the_people_content
		NOT = {
			has_variable = rejected_mheromar
		}
		NOT = {	has_global_variable = mheromar_coup_gladewarden }
		NOT = {	has_global_variable = mheromar_death }
		NOT = {
			any_scope_character = {
				has_variable = mheromar_var
			}
		}
		OR = { # Either The Malachite Knight is "dead" or she is an exile
			NOT = {
				has_global_variable = mheromar_is_alive_global_var
			}
			any_character_in_exile_pool = {
				has_variable = mheromar_var
			}
		}
		OR = {
			AND = { # You're a nation fighting the for the fey or elves (defensive war OR fey aligned revolution/secession)
				OR = {
					has_law = law_type:law_state_religion
					has_law = law_type:law_freedom_of_conscience
					has_law = law_type:law_total_separation
				}
				OR = {
					country_has_state_religion = rel:fey_court
					country_has_state_religion = rel:eordellon
					country_has_state_religion = rel:spring_court
					country_has_state_religion = rel:summer_court
					country_has_state_religion = rel:autumn_court
					country_has_state_religion = rel:winter_court
					country_has_state_religion = rel:vechnogejzn
					any_primary_culture = {	has_discrimination_trait = aelantiri_heritage }
					any_primary_culture = {	has_discrimination_trait = elven_race_heritage }
				}
				OR = {
					AND = {	
						OR = {
							is_revolutionary = yes
							is_secessionist = yes
						}				
						any_diplomatic_play = {
							is_war = yes
							initiator = root
							target = {
								NOT = {
									OR = {
										country_has_state_religion = rel:fey_court
										country_has_state_religion = rel:eordellon
										country_has_state_religion = rel:spring_court
										country_has_state_religion = rel:summer_court
										country_has_state_religion = rel:autumn_court
										country_has_state_religion = rel:winter_court
										country_has_state_religion = rel:vechnogejzn
										any_primary_culture = {	has_discrimination_trait = aelantiri_heritage }
										any_primary_culture = {	has_discrimination_trait = elven_race_heritage }
									}
								}
							}
						}							
					}	
					AND = {
						any_diplomatic_play = {
							is_war = yes
							target = root
							initiator = {
								is_secessionist = no
								# The Malachite Knight should only care to get involved defensively against significant revolutions
								OR = {
									is_revolutionary = no
									country_rank >= rank_value:major_power
								}
								# The Malachite Knight shouldn't be in the business of defending big powers against small ones
								OR = {
									is_revolutionary = yes
									country_rank >= root.country_rank
								}													
								NOT = {
									OR = {
										country_has_state_religion = rel:fey_court
										country_has_state_religion = rel:eordellon
										country_has_state_religion = rel:spring_court
										country_has_state_religion = rel:summer_court
										country_has_state_religion = rel:autumn_court
										country_has_state_religion = rel:winter_court
										country_has_state_religion = rel:vechnogejzn
									}
								}
							}
						}					
					}			
				}
			}
			AND = { # You're fighting for Eordand or Cyranvar
				any_diplomatic_play = {
					is_war = yes
					OR = {
						AND = {
							initiator = {
								OR = {
									any_primary_culture = {	has_discrimination_trait = eordan }
									country_has_primary_culture = cu:wood_elven
								}
								this = root
							}
							target = {
								NOR = {
									any_primary_culture = {	has_discrimination_trait = eordan }
									country_has_primary_culture = cu:wood_elven									
								}
								any_scope_state = {
									state_region = {
										OR = {
											is_homeland = cu:selphereg
											is_homeland = cu:caamas
											is_homeland = cu:peitar
											is_homeland = cu:tuathak
											is_homeland = cu:snecboth
											is_homeland = cu:fograc
											is_homeland = cu:wood_elven
										}
									}
								}
							}
						}
						AND = {
							initiator = {
								OR = {
									any_primary_culture = {	has_discrimination_trait = eordan }
									country_has_primary_culture = cu:wood_elven
								}
								this = root
								is_secessionist = yes
							}
						}						
						AND = {
							target = {
								OR = {
									any_primary_culture = {	has_discrimination_trait = eordan }
									country_has_primary_culture = cu:wood_elven
								}
								this = root
							}
							initiator = {
								NOT = {
									OR = {
										any_primary_culture = {	has_discrimination_trait = eordan }
										country_has_primary_culture = cu:wood_elven
									}			
								}
							}
						}
					}
				}
			}
		}
	}

	immediate = {
		if = {
			limit = {
				NOT = {
					any_character_in_exile_pool = {
						has_variable = mheromar_var
					}
					any_character = {
						has_variable = mheromar_var
					}
				}
			}
			create_character = {
				template = malachite_knight_character_template
				on_created = {
					set_variable = mheromar_var
					set_variable = {
						name = mheromar_cooldown_var
						months = short_modifier_time
					}
					save_scope_as = mheromar_scope
					set_character_immortal = yes
				}
			}
			set_global_variable = mheromar_is_alive_global_var
		}
		else_if = {
			random_character_in_exile_pool = {
				limit = {
					has_variable = mheromar_var
				}
				#transfer_character = root
				#save_scope_as = mheromar_scope
				#set_variable = {
				#	name = mheromar_cooldown_var
				#	months = short_modifier_time
				#}
				#While fem general still bugged we have to kill and respawn her
				kill_character = yes
				create_character = {
					template = malachite_knight_character_template
					on_created = {
						set_variable = mheromar_var
						set_variable = {
							name = mheromar_cooldown_var
							months = short_modifier_time
						}
						save_scope_as = mheromar_scope
						set_character_immortal = yes
					}
				}
				set_global_variable = mheromar_is_alive_global_var
			}
		}
		random_scope_character = {
            limit = {
                has_variable = mheromar_var
            }
            save_scope_as = mheromar_scope
        }
		if = {
			limit = { exists = global_var:mheromar_fame_var }
			change_global_variable = {
				name = mheromar_fame_var
				add = 1
			}
		}
		else = {
			set_global_variable = {
				name = mheromar_fame_var
				value = 1
			}
		}
		if = {
			limit = {
				any_diplomatic_play = {
					is_war = yes
					target = root
				}
			}
			random_diplomatic_play = {
				limit = {
					is_war = yes
					target = root					
				}
				initiator = {
					save_scope_as = mheromar_spawn_foe_scope
				}
			}
		}
		else_if = {
			limit = {
				any_diplomatic_play = {
					is_war = yes
					initiator = root
				}
			}
			random_diplomatic_play = {
				limit = {
					is_war = yes
					initiator = root					
				}
				target = {
					save_scope_as = mheromar_spawn_foe_scope
				}
			}			
		}
		save_scope_as = mheromar_new_home
	}

	cancellation_trigger = {
		OR = {
			NOT = {
				exists = scope:mheromar_scope
			}
			scope:mheromar_scope = {
				is_character_alive = no
			}
		}
	}

	option = {
		name = mheromar_events.1.a
		default_option = yes
		#scope:mheromar_scope ?= {
		#	add_character_role = general
		#}
		if = {
			limit = {
				global_var:mheromar_fame_var = 1
				scope:mheromar_scope ?= { 
					NOT = {
						has_modifier = mheromar_popularity_modifier_tier_1 
					}
				}
			}
			scope:mheromar_scope ?= {
				add_modifier = mheromar_popularity_modifier_tier_1
			}
		}
		if = {
			limit = {
				global_var:mheromar_fame_var = 2
				scope:mheromar_scope ?= {
					NOT = { 
						has_modifier = mheromar_popularity_modifier_tier_2
					}
				}
			}
			scope:mheromar_scope ?= {
				if = {
					limit = {
						has_modifier = mheromar_popularity_modifier_tier_1
					}
					hidden_effect = { 
						remove_modifier = mheromar_popularity_modifier_tier_1
					}
				}
				add_modifier = mheromar_popularity_modifier_tier_2
			}
		}
		if = {
			limit = {
				global_var:mheromar_fame_var = 3
				scope:mheromar_scope ?= {
					NOT = { 
						has_modifier = mheromar_popularity_modifier_tier_3
					}
				}
			}
			scope:mheromar_scope ?= {
				if = {
					limit = {
						has_modifier = mheromar_popularity_modifier_tier_2
					}
					hidden_effect = { 
						remove_modifier = mheromar_popularity_modifier_tier_2
					}
				}
				add_modifier = mheromar_popularity_modifier_tier_3
			}
		}
		if = {
			limit = {
				global_var:mheromar_fame_var = 4
				scope:mheromar_scope ?= {
					NOT = { 
						has_modifier = mheromar_popularity_modifier_tier_4
					}
				}
			}
			scope:mheromar_scope ?= {
				if = {
					limit = {
						has_modifier = mheromar_popularity_modifier_tier_3
					}
					hidden_effect = { 
						remove_modifier = mheromar_popularity_modifier_tier_3
					}
				}
				add_modifier = mheromar_popularity_modifier_tier_4
			}
		}
		if = {
			limit = {
				global_var:mheromar_fame_var >= 5
				scope:mheromar_scope ?= {
					NOT = { 
						has_modifier = mheromar_popularity_modifier_tier_5
					}
				}
			}
			scope:mheromar_scope ?= {
				if = {
					limit = {
						has_modifier = mheromar_popularity_modifier_tier_4
					}
					hidden_effect = { 
						remove_modifier = mheromar_popularity_modifier_tier_4
					}
				}
				add_modifier = mheromar_popularity_modifier_tier_5
			}
		}
		if = {
			limit = {
				global_var:mheromar_fame_var >= 2
				scope:mheromar_scope ?= {
					has_trait = basic_offensive_planner
				}
			}
			scope:mheromar_scope ?= {
				random_list = {
					50 = {
						hidden_effect = { 
							remove_trait = basic_offensive_planner
						}
						add_trait = experienced_offensive_planner
					}
					50 = {
					}
				}
			}
		}
		if = {
			limit = {
				global_var:mheromar_fame_var >= 4
				scope:mheromar_scope ?= {
					has_trait = experienced_offensive_planner
				}
			}
			scope:mheromar_scope ?= {
				random_list = {
					50 = {
						hidden_effect = { 
							remove_trait = experienced_offensive_planner
						}
						add_trait = expert_offensive_planner
					}
					50 = {
					}
				}
			}
		}

		every_country = {
			limit = {
				OR = {
					ROOT = {
						is_player = yes
					}
					has_diplomatic_relevance = ROOT
					has_variable = mheromar_has_been_here
				}
			}
			post_notification = mheromar_is_here
		}

		hidden_effect = {
			if = {
				limit = {
					NOT = { has_variable = mheromar_has_been_here }
				}
				set_variable = mheromar_has_been_here
			}
		}
	}
	option = {
		name = mheromar_events.1.b
		set_variable = rejected_mheromar
		if = {
			limit = {
				any_interest_group = {
					OR = {
						is_interest_group_type = ig_rural_folk
						is_interest_group_type = ig_trade_unions
						is_interest_group_type = ig_intelligentsia
						is_interest_group_type = ig_petty_bourgeoisie
						is_interest_group_type = ig_armed_forces
					}
					is_marginal = no
					leader = {
						OR = {
							has_ideology = ideology:ideology_radical
							has_ideology = ideology:ideology_social_democrat
							has_ideology = ideology:ideology_communist
							has_ideology = ideology:ideology_republican_leader
						}
					}
				}
			}
			every_interest_group = {
				limit = {
					OR = {
						is_interest_group_type = ig_rural_folk
						is_interest_group_type = ig_trade_unions
						is_interest_group_type = ig_intelligentsia
						is_interest_group_type = ig_petty_bourgeoisie
						is_interest_group_type = ig_armed_forces
					}
					is_marginal = no
					leader = {
						OR = {
							has_ideology = ideology:ideology_radical
							has_ideology = ideology:ideology_social_democrat
							has_ideology = ideology:ideology_communist
							has_ideology = ideology:ideology_republican_leader
						}
					}
				}
				add_modifier = {
					name = rejected_mheromar_modifier
					months = normal_modifier_time
				}
			}
		}
		if = {
			limit = {
				any_scope_character = {
					has_variable = mheromar_var
				}
			}
			scope:mheromar_scope ?= {
				#exile_character = yes
				#clean_up_exile_character_roles = yes
				kill_character = yes
			}
			remove_global_variable = mheromar_is_alive_global_var
		}
	}
}
# mheromar Fucks Off
mheromar_events.2 = {
	type = country_event
	placement = root
	

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/event_newspaper.dds"

	title = mheromar_events.2.t
	desc = {
		first_valid = {
			triggered_desc = {
				desc = mheromar_events.2.d_1
				trigger = {
					any_diplomatic_play = {
						is_war = yes
						OR = {
							AND = {
								initiator = {
									OR = {
										any_primary_culture = {	has_discrimination_trait = eordan }
										country_has_primary_culture = cu:wood_elven
									}
									NOT = { 
										has_variable = rejected_mheromar 
									}
								}
								target = {
									NOT = {
										OR = {
											any_primary_culture = {	has_discrimination_trait = eordan }
											country_has_primary_culture = cu:wood_elven
										}			
									}
									any_scope_state = {
										state_region = {
											OR = {
												is_homeland = cu:selphereg
												is_homeland = cu:caamas
												is_homeland = cu:peitar
												is_homeland = cu:tuathak
												is_homeland = cu:snecboth
												is_homeland = cu:fograc
												is_homeland = cu:wood_elven
											}
										}
									}
								}
							}
							AND = {
								target = {
									any_primary_culture = {
										has_discrimination_trait = eordan
									}
									NOT = { 
										has_variable = rejected_mheromar 
									}
								}
								initiator = {
									NOT = {
										OR = {
											any_primary_culture = {	has_discrimination_trait = eordan }
											country_has_primary_culture = cu:wood_elven
										}				
									}
								}
							}
						}
					}
				}
			}
			triggered_desc = {
				desc = mheromar_events.2.d_2
				trigger = {
					NOT = {
						any_diplomatic_play = {
							is_war = yes
							OR = {
								AND = {
									initiator = {
										OR = {
											any_primary_culture = {	has_discrimination_trait = eordan }
											country_has_primary_culture = cu:wood_elven
										}	
										NOT = { 
											has_variable = rejected_mheromar 
										}
									}
									target = {
										NOT = {
											OR = {
												any_primary_culture = {	has_discrimination_trait = eordan }
												country_has_primary_culture = cu:wood_elven
											}				
										}
										any_scope_state = {
											state_region = {
												OR = {
													is_homeland = cu:selphereg
													is_homeland = cu:caamas
													is_homeland = cu:peitar
													is_homeland = cu:tuathak
													is_homeland = cu:snecboth
													is_homeland = cu:fograc
													is_homeland = cu:wood_elven
												}
											}
										}
									}
								}
								AND = {
									target = {
										OR = {
											any_primary_culture = {	has_discrimination_trait = eordan }
											country_has_primary_culture = cu:wood_elven
										}	
										NOT = { 
											has_variable = rejected_mheromar 
										}
									}
									initiator = {
										NOT = {
											OR = {
												any_primary_culture = {	has_discrimination_trait = eordan }
												country_has_primary_culture = cu:wood_elven
											}				
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	flavor = mheromar_events.2.f

	cooldown = { months = short_modifier_time }

	duration = 3

	dlc = dlc004

	trigger = {
		has_dlc_feature = voice_of_the_people_content
		any_scope_character = {
			has_variable = mheromar_var
		}
		NOT = {	has_global_variable = mheromar_coup_gladewarden }
		NOT = {	has_global_variable = mheromar_death }
		OR = {
			AND = {
				is_at_war = no
				NOR = { 
					c:B87 ?= this
					any_primary_culture = {	has_discrimination_trait = eordan }
					country_has_primary_culture = cu:wood_elven
				}
				any_scope_character = {
					has_variable = mheromar_var
					NOT = {
						has_variable = mheromar_cooldown_var
					}
				}
			}
			AND = {
				any_diplomatic_play = {
					is_war = yes
					OR = {
						AND = {
							initiator = {
								NOT = { this = root }
								OR = {
									any_primary_culture = {	has_discrimination_trait = eordan }
									country_has_primary_culture = cu:wood_elven
								}
								NOT = { 
									has_variable = rejected_mheromar 
								}
							}
							target = {
								NOT = {
									OR = {
										any_primary_culture = {	has_discrimination_trait = eordan }
										country_has_primary_culture = cu:wood_elven
									}		
								}
								any_scope_state = {
									state_region = {
										OR = {
											is_homeland = cu:selphereg
											is_homeland = cu:caamas
											is_homeland = cu:peitar
											is_homeland = cu:tuathak
											is_homeland = cu:snecboth
											is_homeland = cu:fograc
											is_homeland = cu:wood_elven
										}
									}
								}
							}
						}
						AND = {
							target = {
								NOT = { this = root }
								OR = {
									country_has_state_religion = rel:fey_court
									country_has_state_religion = rel:eordellon
									country_has_state_religion = rel:spring_court
									country_has_state_religion = rel:summer_court
									country_has_state_religion = rel:autumn_court
									country_has_state_religion = rel:winter_court
									country_has_state_religion = rel:vechnogejzn
									any_primary_culture = {	has_discrimination_trait = aelantiri_heritage }
									any_primary_culture = {	has_discrimination_trait = elven_race_heritage }
								}
								NOT = { 
									has_variable = rejected_mheromar 
								}
							}
							initiator = {
								NOT = {
									OR = {
										country_has_state_religion = rel:fey_court
										country_has_state_religion = rel:eordellon
										country_has_state_religion = rel:spring_court
										country_has_state_religion = rel:summer_court
										country_has_state_religion = rel:autumn_court
										country_has_state_religion = rel:winter_court
										country_has_state_religion = rel:vechnogejzn
									}			
								}
							}
						}
					}
				}
			}
		}
	}

	cancellation_trigger = {
		OR = {
			NOT = {
				exists = scope:mheromar_scope
			}
			scope:mheromar_scope = {
				is_character_alive = no
			}
		}
	}

	immediate = {
		random_scope_character = {
			limit = {
				has_variable = mheromar_var
			}
			save_scope_as = mheromar_scope
		}
		if = {
			limit = {
				any_diplomatic_play = {
					is_war = yes
					target = {
						NOT = { this = root }
						OR = {
							country_has_state_religion = rel:fey_court
							country_has_state_religion = rel:eordellon
							country_has_state_religion = rel:spring_court
							country_has_state_religion = rel:summer_court
							country_has_state_religion = rel:autumn_court
							country_has_state_religion = rel:winter_court
							country_has_state_religion = rel:vechnogejzn
							any_primary_culture = {	has_discrimination_trait = aelantiri_heritage }
							any_primary_culture = {	has_discrimination_trait = elven_race_heritage }
						}
					}
					initiator = {
						NOT = {
							OR = {
								country_has_state_religion = rel:fey_court
								country_has_state_religion = rel:eordellon
								country_has_state_religion = rel:spring_court
								country_has_state_religion = rel:summer_court
								country_has_state_religion = rel:autumn_court
								country_has_state_religion = rel:winter_court
								country_has_state_religion = rel:vechnogejzn
							}		
						}
					}
				}
			}
			random_diplomatic_play = {
				limit = {
					is_war = yes
					target = {
						NOT = { this = root }
						OR = {
							country_has_state_religion = rel:fey_court
							country_has_state_religion = rel:eordellon
							country_has_state_religion = rel:spring_court
							country_has_state_religion = rel:summer_court
							country_has_state_religion = rel:autumn_court
							country_has_state_religion = rel:winter_court
							country_has_state_religion = rel:vechnogejzn
							any_primary_culture = {	has_discrimination_trait = aelantiri_heritage }
							any_primary_culture = {	has_discrimination_trait = elven_race_heritage }
						}
					}
					initiator = {
						NOT = {
							OR = {
								country_has_state_religion = rel:fey_court
								country_has_state_religion = rel:eordellon
								country_has_state_religion = rel:spring_court
								country_has_state_religion = rel:summer_court
								country_has_state_religion = rel:autumn_court
								country_has_state_religion = rel:winter_court
								country_has_state_religion = rel:vechnogejzn
							}			
						}
					}	
				}
				target = {
					save_scope_as = mheromar_eordan_nation_scope
				}
				initiator = {
					save_scope_as = mheromar_foreign_nation_scope
				}
			}
		}
		else_if = {
			limit = {
				any_diplomatic_play = {
					is_war = yes
					initiator = {
						NOT = { this = root }
						OR = {
							any_primary_culture = {	has_discrimination_trait = eordan }
							country_has_primary_culture = cu:wood_elven
						}	
					}
					target = {
						NOT = {
							OR = {
								any_primary_culture = {	has_discrimination_trait = eordan }
								country_has_primary_culture = cu:wood_elven
							}			
						}
						any_scope_state = {
							state_region = {
								OR = {
									is_homeland = cu:selphereg
									is_homeland = cu:caamas
									is_homeland = cu:peitar
									is_homeland = cu:tuathak
									is_homeland = cu:snecboth
									is_homeland = cu:fograc
									is_homeland = cu:wood_elven
								}
							}
						}
					}
				}
			}
			random_diplomatic_play = {
				limit = {
					is_war = yes
					initiator = {
						NOT = { this = root }
						OR = {
							any_primary_culture = {	has_discrimination_trait = eordan }
							country_has_primary_culture = cu:wood_elven
						}
					}
					target = {
						NOT = {
							OR = {
								any_primary_culture = {	has_discrimination_trait = eordan }
								country_has_primary_culture = cu:wood_elven
							}			
						}
						any_scope_state = {
							state_region = {
								OR = {
									is_homeland = cu:selphereg
									is_homeland = cu:caamas
									is_homeland = cu:peitar
									is_homeland = cu:tuathak
									is_homeland = cu:snecboth
									is_homeland = cu:fograc
									is_homeland = cu:wood_elven
								}
							}
						}
					}	
				}
				initiator = {
					save_scope_as = mheromar_eordan_nation_scope
				}
				target = {
					save_scope_as = mheromar_foreign_nation_scope
				}
			}
		}
		save_scope_as = mheromar_old_home
	}

	option = {
		name = mheromar_events.2.a
		default_option = yes
		if = {
			limit = {
				scope:mheromar_scope = {
					has_modifier = eordan_legionaries_modifier
				}
			}
			scope:mheromar_scope = {
				remove_modifier = eordan_legionaries_modifier
			}
		}
		if = {
			limit = {
				any_scope_building = {
					has_modifier = eordan_legion_barracks_modifier
				}
			}
			every_scope_building = {
				limit = {
					has_modifier = eordan_legion_barracks_modifier
				}
				remove_modifier = eordan_legion_barracks_modifier
			}
		}
		scope:mheromar_scope = {
			#exile_character = yes
			#clean_up_exile_character_roles = yes
			kill_character = yes
		}
		remove_global_variable = mheromar_is_alive_global_var
		if = {
			limit = {
				any_diplomatic_play = {
					is_war = yes
					target = {
						NOT = { this = root }
						OR = {
							country_has_state_religion = rel:fey_court
							country_has_state_religion = rel:eordellon
							country_has_state_religion = rel:spring_court
							country_has_state_religion = rel:summer_court
							country_has_state_religion = rel:autumn_court
							country_has_state_religion = rel:winter_court
							country_has_state_religion = rel:vechnogejzn
							any_primary_culture = {	has_discrimination_trait = aelantiri_heritage }
							any_primary_culture = {	has_discrimination_trait = elven_race_heritage }
						}
					}
					initiator = {
						NOT = {
							OR = {
								country_has_state_religion = rel:fey_court
								country_has_state_religion = rel:eordellon
								country_has_state_religion = rel:spring_court
								country_has_state_religion = rel:summer_court
								country_has_state_religion = rel:autumn_court
								country_has_state_religion = rel:winter_court
								country_has_state_religion = rel:vechnogejzn
							}			
						}
					}
				}
			}
			random_diplomatic_play = {
				limit = {
					is_war = yes
					target = {
						NOT = { this = root }
						OR = {
							country_has_state_religion = rel:fey_court
							country_has_state_religion = rel:eordellon
							country_has_state_religion = rel:spring_court
							country_has_state_religion = rel:summer_court
							country_has_state_religion = rel:autumn_court
							country_has_state_religion = rel:winter_court
							country_has_state_religion = rel:vechnogejzn
							any_primary_culture = {	has_discrimination_trait = aelantiri_heritage }
							any_primary_culture = {	has_discrimination_trait = elven_race_heritage }
						}
					}
					initiator = {
						NOT = {
							OR = {
								country_has_state_religion = rel:fey_court
								country_has_state_religion = rel:eordellon
								country_has_state_religion = rel:spring_court
								country_has_state_religion = rel:summer_court
								country_has_state_religion = rel:autumn_court
								country_has_state_religion = rel:winter_court
								country_has_state_religion = rel:vechnogejzn
							}			
						}
					}	
				}
				target = {
					trigger_event = mheromar_events.1
				}
			}
		}
		else_if = {
			limit = {
				any_diplomatic_play = {
					is_war = yes
					initiator = {
						NOT = { this = root }
						OR = {
							any_primary_culture = {	has_discrimination_trait = eordan }
							country_has_primary_culture = cu:wood_elven
						}
					}
					target = {
						NOT = {
							OR = {
								any_primary_culture = {	has_discrimination_trait = eordan }
								country_has_primary_culture = cu:wood_elven
							}			
						}
						any_scope_state = {
							state_region = {
								OR = {
									is_homeland = cu:selphereg
									is_homeland = cu:caamas
									is_homeland = cu:peitar
									is_homeland = cu:tuathak
									is_homeland = cu:snecboth
									is_homeland = cu:fograc
									is_homeland = cu:wood_elven
								}
							}
						}
					}
				}
			}
			random_diplomatic_play = {
				limit = {
					is_war = yes
					initiator = {
						NOT = { this = root }
						OR = {
							any_primary_culture = {	has_discrimination_trait = eordan }
							country_has_primary_culture = cu:wood_elven
						}
					}
					target = {
						NOT = {
							OR = {
								any_primary_culture = {	has_discrimination_trait = eordan }
								country_has_primary_culture = cu:wood_elven
							}			
						}
						any_scope_state = {
							state_region = {
								OR = {
									is_homeland = cu:selphereg
									is_homeland = cu:caamas
									is_homeland = cu:peitar
									is_homeland = cu:tuathak
									is_homeland = cu:snecboth
									is_homeland = cu:fograc
									is_homeland = cu:wood_elven
								}
							}
						}
					}	
				}
				initiator = {
					trigger_event = mheromar_events.1
				}
			}
		}

		every_country = {
			limit = {
				OR = {
					ROOT = {
						is_player = yes
					}
					has_diplomatic_relevance = ROOT
					has_variable = mheromar_has_been_here
					scope:mheromar_eordan_nation_scope ?= this
					scope:mheromar_foreign_nation_scope ?= this
				}
			}
			post_notification = mheromar_skedaddles
		}
	}
}
# eordan Legion of Volunteers
mheromar_events.3 = {
	type = country_event
	placement = scope:eordan_legion_state_scope
	

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/event_military.dds"

	title = mheromar_events.3.t
	desc = mheromar_events.3.d
	flavor = mheromar_events.3.f

	duration = 3

	dlc = dlc004

	trigger = {
		NOT = {
			has_variable = eordan_legion_var
		}
		has_dlc_feature = voice_of_the_people_content
		any_scope_character = {
			has_variable = mheromar_var
		}
		NOT = {	has_global_variable = mheromar_coup_gladewarden }
		NOT = {	has_global_variable = mheromar_death }
		any_scope_state = {
			any_scope_pop = {
				culture = {
					has_discrimination_trait = eordan
				}
			}
		}
		any_diplomatic_play = {
			is_war = yes
			OR = {
				initiator = root
				target = root
			}
		}
	}

	immediate = {
		set_variable = eordan_legion_var
		random_scope_character = {
			limit = {
				has_role = general
				has_variable = mheromar_var
			}
			save_scope_as = mheromar_scope
		}
		random_scope_state = {
			limit = {
				any_scope_pop = {
					culture = {
						has_discrimination_trait = eordan
					}
				}
			}
			save_scope_as = eordan_legion_state_scope
		}
		if = {
			limit = {
				scope:eordan_legion_state_scope = {
					any_scope_building = {
						is_building_type = building_barracks
					}
				}
			}
			scope:eordan_legion_state_scope = {
				random_scope_building = {
					limit = {
						is_building_type = building_barracks
					}
					save_scope_as = eordan_legion_barracks_scope
				}
			}
		}
		if = {
			limit = {
				any_diplomatic_play = {
					is_war = yes
					target = root
				}
			}
			random_diplomatic_play = {
				limit = {
					is_war = yes
					target = root					
				}
				initiator = {
					save_scope_as = eordan_legion_foe_scope
				}
			}
		}
		else_if = {
			limit = {
				any_diplomatic_play = {
					is_war = yes
					initiator = root
				}
			}
			random_diplomatic_play = {
				limit = {
					is_war = yes
					initiator = root					
				}
				target = {
					save_scope_as = eordan_legion_foe_scope
				}
			}			
		}
	}

	option = {
		name = mheromar_events.3.a
		default_option = yes
		scope:mheromar_scope = {
			add_modifier = {
				name = eordan_legionaries_modifier
				months = normal_modifier_time
			}
		}	
	}
	
	option = {
		name = mheromar_events.3.b
		trigger = {
			scope:eordan_legion_state_scope = {
				any_scope_building = {
					is_building_type = building_barracks
				}
			}
		}
		scope:eordan_legion_barracks_scope = {
			add_modifier = {
				name = eordan_legion_barracks_modifier
				months = normal_modifier_time
			}
		}
	}
}
# mheromar Riles Up Peasants
mheromar_events.4 = {
	type = country_event
	placement = root
	

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/event_protest.dds"

	title = mheromar_events.4.t
	desc = mheromar_events.4.d
	flavor = mheromar_events.4.f

	cooldown = { months = normal_modifier_time }

	duration = 3

	dlc = dlc004

	trigger = {
		has_dlc_feature = voice_of_the_people_content
		any_scope_character = {
			has_variable = mheromar_var
		}
		any_scope_state = {
			is_incorporated = yes
			any_scope_pop = {
				is_pop_type = farmers
			}
		}
		NOT = {	has_global_variable = mheromar_coup_gladewarden }
		NOT = {	has_global_variable = mheromar_death }
		any_diplomatic_play = {
			is_war = yes
			any_scope_play_involved = {
				has_war_with = root
			}
		}
		ig:ig_rural_folk = {
			is_marginal	= no
		}
	}

	immediate = {
		random_scope_character = {
			limit = {
				has_variable = mheromar_var
			}
			save_scope_as = mheromar_scope
		}
		random_scope_state = {
			limit = {
				is_incorporated = yes
					any_scope_pop = {
					is_pop_type = farmers
				}
			}
			save_scope_as = mheromar_peasants_state_scope
		}
		random_diplomatic_play = {
			limit = {
				is_war = yes
				any_scope_play_involved = {
					has_war_with = root
				}
			}
			random_scope_play_involved = {
				limit = {
					has_war_with = root
				}
				save_scope_as = mheromar_peasants_foe_scope
			}
		}
	}

	option = {
		name = mheromar_events.4.a
		default_option = yes
		ig:ig_rural_folk = {
			add_modifier = {
				name = emboldened_peasantry_modifier
				months = normal_modifier_time
			}
		}
		scope:mheromar_peasants_state_scope = {
			add_radicals_in_state = {
				pop_type = farmers
				value = small_radicals
			}
		}
	}

	option = {
		name = mheromar_events.4.b
		highlighted_option = yes
		random_list = {
			80 = {
				scope:mheromar_scope = {
					#exile_character = yes
					#clean_up_exile_character_roles = yes
					kill_character = yes
				}
				set_variable = rejected_mheromar
				remove_global_variable = mheromar_is_alive_global_var
			}
			20 = {
			}
		}
	}
}


# mheromar dies unification successful
mheromar_events.5 = {
	type = country_event
	placement = root
	

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/event_protest.dds"

	title = mheromar_events.5.t
	desc = mheromar_events.5.d
	flavor = mheromar_events.5.f

	duration = 3

	trigger = {
	}

	immediate = {
		random_character = {
			limit = {
				has_variable = mheromar_var
			}
			kill_character = yes
		}
		remove_global_variable = mheromar_is_alive_global_var
		set_global_variable = mheromar_death
	}

	option = {
		name = mheromar_events.5.a
		default_option = yes
	}

}

# mheromar dies failed coup version
mheromar_events.6 = {
	type = country_event
	placement = root
	

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/event_protest.dds"

	title = mheromar_events.6.t
	desc = mheromar_events.6.d
	flavor = mheromar_events.6.f

	duration = 3

	dlc = dlc004

	trigger = {
	}

	immediate = {
		c:B86B = {
			random_scope_character = {
				limit = {
					has_variable = mheromar_var
				}
				kill_character = yes
			}
		}
		remove_global_variable = mheromar_is_alive_global_var
		set_global_variable = mheromar_death
	}

	option = {
		name = mheromar_events.6.a
		default_option = yes
	}

}

# mheromar must go her planets needs her
mheromar_events.7 = {
	type = country_event
	placement = root
	

	on_created_soundeffect = "event:/SFX/UI/Alerts/event_appear"

	icon = "gfx/interface/icons/event_icons/event_protest.dds"

	title = mheromar_events.7.t
	desc = mheromar_events.7.d
	flavor = mheromar_events.7.f

	duration = 1

	dlc = dlc004

	trigger = {
	}

	immediate = {
	}

	option = {
		name = mheromar_events.7.a
		default_option = yes
	}

}
