﻿# Strategic Regions in Western Europe

region_south_rahen = {
	graphical_culture = "arabic"
	capital_province = "xd4b22a" #Dhenijansar
	map_color = { 80 0 100 }
	states = { STATE_AVHAVUBHIYA STATE_IYARHASHAR STATE_SOUTH_GHANKEDHEN STATE_NORTH_GHANKEDHEN STATE_TUDHINA STATE_PASIRAGHA STATE_UPPER_DHENBASANA STATE_LOWER_DHENBASANA STATE_ASCENSION_JUNGLE STATE_TUJGAL STATE_SATARSAYA STATE_TIPHIYA }
}

region_north_rahen = {
	graphical_culture = "arabic"
	capital_province = "x183a9a" #Khamapar
	map_color = { 180 10 5 }
	states = { STATE_BABHAGAMA STATE_WEST_GHAVAANAJ STATE_EAST_GHAVAANAJ STATE_TUGHAYASA STATE_TILTAGHAR STATE_WEST_NADIMRAJ STATE_RAJNADHAGA STATE_CENTRAL_NADIMRAJ STATE_EAST_NADIMRAJ STATE_HOBGOBLIN_HOMELANDS STATE_GHILAKHAD STATE_RAGHAJANDI STATE_GHATASAK STATE_SHAMAKHAD_PLAINS }
}

region_kharunyana = {
	graphical_culture = "arabic"
	capital_province = "x4aa200" #Sarisung
	map_color = { 200 240 180 }
	states = { STATE_DHUJAT STATE_SARISUNG STATE_SRAMAYA STATE_KHARUNYANA_BOMDAN STATE_JIEZHONG }
}

region_bomdan = {
	graphical_culture = "arabic"
	capital_province = "xe3107c" #Bim Lau
	map_color = { 0 50 10 }
	states = { STATE_LOWER_TELEBEI STATE_BIM_LAU STATE_DEKPHRE STATE_KHINDI STATE_KHABTEI_TELENI STATE_RONGBEK STATE_NAGON }
}

region_thidinkai = {
	graphical_culture = "asian"
	capital_province = "x12f9a4" #
	map_color = { 255 174 0 }
	states = { STATE_SIKAI STATE_HINPHAT STATE_KHOM_MA STATE_PHONAN STATE_SIRTAN STATE_KUDET_KAI STATE_YEMAKAIBO STATE_BINHRUNGHIN STATE_VERKAL_OZOVAR }
}

region_lupulan = {
	graphical_culture = "asian"
	capital_province = "xb95ea5" #
	map_color = { 62 180 137 }
	states = { STATE_HOANGDESINH STATE_TLAGUKIT STATE_ARAWKELIN STATE_REWIRANG STATE_MESATULEK STATE_NON_CHIEN }
}

region_ringed_isles = {
	graphical_culture = "asian"
	capital_province = "x50A9C1" #
	map_color = { 255 210 34 }
	states = { STATE_KIUBANG STATE_GINHYUT STATE_LUNGDOU STATE_LEOIHOIN }
}

region_west_yanshen = {
	graphical_culture = "asian"
	capital_province = "xfbffce" #
	map_color = { 60 10 0 }
	states = { STATE_SIR STATE_QIANZHAOLIN STATE_WANGQIU STATE_MOGUTIAN STATE_BIANYUAN STATE_EBYANFANGU STATE_BALRIJIN STATE_YANSZIN }
}

region_east_yanshen = {
	graphical_culture = "asian"
	capital_province = "x40e0d4" #Tianlou
	map_color = { 156 172 171 }
	states = { STATE_ZYUJYUT STATE_LUOYIP STATE_HUNGNGON STATE_IONGSIM STATE_LINGYUK STATE_LIUMINXIANG STATE_TIANLOU STATE_XUANBING STATE_JINJIANG }
}

region_north_yanshen = {
	graphical_culture = "asian"
	capital_province = "x98285a" #
	map_color = { 11 17 69 }
	states = { STATE_SUGROLTONA STATE_MORYOKANG STATE_JIANTSIANG STATE_YUNGHUUN STATE_NUUGDAN_TSARAI STATE_RUNG_OEDEK STATE_ONDEOG STATE_JIBOJAGI STATE_SUPPIU }
}

region_moduk = {
	graphical_culture = "asian"
	capital_province = "x736B75" #Outpost
	map_color = { 110 17 69 }
	states = { STATE_BAGDAGERIN STATE_TUUSBULAK STATE_ANARKHUDI STATE_DOLSHOB STATE_NURBOESIG STATE_OINUKHUDI STATE_SOMGYONGHON }
}

region_nomsyulhan = {
	graphical_culture = "asian"
	capital_province = "x243483" #
	map_color = { 210 107 0 }
	states = { STATE_UPPER_KHARUNYANA_BASIN STATE_GREENVALE STATE_NOMSYULHANI_BADLANDS STATE_HAKUURUK STATE_CENTRAL_KHAASHALG }
}