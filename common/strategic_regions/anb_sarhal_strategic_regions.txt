﻿region_kheterata = {
	capital_province = "xC35600" #Kheterat
	map_color = { 194 189 47 }
	states = { STATE_COAST_OF_TEARS STATE_IBTAT STATE_AWAASHESH STATE_RASUSOPOT STATE_SHASOURA STATE_HOHKROGAR STATE_MOTHERS_DELTA STATE_LOWER_SORROW STATE_KHETERAT STATE_WESTERN_SALAHAD }
}
region_irsmahapan_plains = {
	capital_province = "x0C649F" #someplace in Irsmahap, will find actual city later
	map_color = { 140 113 36 }
	states = {  STATE_UPPER_SORROW STATE_GOLKORA STATE_IRSMAHAP STATE_NIRAMIT STATE_HAPGESH  STATE_CENTRAL_SALAHAD STATE_GRIZAKEKAL }
}
region_akasik = {
	capital_province = "x05AF04" #random province in khasa
	map_color = { 227 128 30 }
	states = { STATE_DESHAK STATE_KHASA STATE_EKHA STATE_AKASIK_MOUNTAINS STATE_NORTHERN_SANDSPITE STATE_WANDERERS_ISLES }
}

region_horashesh = {
	capital_province = "x799A8D" #Kuika Spire
	map_color = { 123 189 149 }
	states = { STATE_MUKIS STATE_IMIDKIS STATE_ZUVAVIM STATE_ZENA STATE_MUZINGRAZI STATE_IRIKIN STATE_YADATI STATE_KOGINGASA STATE_SAQRAXAT STATE_KHAWIT STATE_IRSUKSHESH }
}

region_gazraak = {
	capital_province = "x4DB0EE" #Kvangraak
	map_color = { 117 134 143 }
	states = {  STATE_KOGXUL STATE_NARAKUKKA STATE_GARPIXEQQA STATE_MAZEXHAD STATE_DUWARKANI STATE_KOGZALLA STATE_KOGGRAFFA STATE_KVANGRAAK STATE_GIZAN STATE_FIAREKAL STATE_DEMON_SANDS }
}

region_west_madriamilak = {
	capital_province = "x6444F3" #random province in Merekawi
	map_color = { 8 60 64 }
	states = { STATE_KIHEREGA STATE_MEWOJALA STATE_MEREKAWI STATE_WEDLAYAWET STATE_YETEDARA STATE_MELAKHAKI STATE_RUGRUSHYABA STATE_IWIXARG STATE_BERPARWEDEB STATE_HISOST }
}

region_east_madriamilak = {
	capital_province = "x02F7D0" #Crater
	map_color = { 42 119 125 }
	states = { STATE_FEDONI STATE_SIMAHRIKA STATE_NALENI STATE_CHAFALETI STATE_TEDALOK STATE_ZEDAHRIKA STATE_MARADEFI STATE_AFERET STATE_TELEKIBE STATE_FOG_ISLES }
}

region_bamashyaba = {
	capital_province = "x35E0B0" #port in Bikmeghome
	map_color = { 100 189 138 }
	states = { STATE_MVUALIMA STATE_MASHREMBTA STATE_JUUDHULA STATE_KOMBEDHULA STATE_UMUPKA STATE_BIKMEGHOME STATE_UMUZOSI STATE_BIMBIGIHU STATE_KASHYZIR }
}

region_adzalas_gulf = {
	capital_province = "x6DB940" #port in Khugra
	map_color = { 42 209 113 }
	states = { STATE_IRKLAZ STATE_KARASSK STATE_RANKATY_ISLANDS STATE_KHUGRA STATE_NAZHNI STATE_YASSA STATE_TRESSIK_ISLANDS STATE_ZULBUR }
}

region_taneyas = {
	capital_province = "xE09953" #Zerat
	map_color = { 10 64 33 }
	states = { STATE_AZASI STATE_ANZANES STATE_SUTNASAAK STATE_LASADZA STATE_NATALAZ STATE_ZALKATA STATE_ADGUR STATE_ZATSUTI STATE_NINUNITH STATE_TAZAS STATE_YADASADZA STATE_KHUSADZA }
}


region_shadowswamp = {
	capital_province = "xDB2002" #The tree
	map_color = { 40 40 40 }
	states = { STATE_TAZOINE STATE_MYPODAUD STATE_KAGDILGIR STATE_AAKEIRITKI }
}

region_fahvanosy = {
	capital_province = "xCA0101" #random province
	map_color = { 62 60 181 }
	states = { STATE_AFOMAPOBO STATE_NAFALOFIMAPO STATE_MAPONOSY STATE_HELIBETIBOKBO STATE_DRONGORONSI STATE_SISINYUYE STATE_TVARASTAMANOSY STATE_TERAMZADAI STATE_SIL_BERKHENS_LAND }
}

region_jasiir_jadid = {
	capital_province = "xE9D8FA" #random province
	map_color = { 11 50 112 }
	states = { STATE_ASHAMADIAT STATE_DADUBAKHA STATE_GAWAKHIZA STATE_MADHALAYSNIMO STATE_BEERAGGA STATE_DIMDHULKA STATE_BADDABI STATE_BADABHUR STATE_UWAKILA_BAASHI }
}

region_ardimya = {
	capital_province = "x7D9E17" #random province
	map_color = { 217 207 15 }
	states = { STATE_DJINNCOST STATE_OHITS STATE_WATTOI STATE_CADIHLAND STATE_SOUTHERN_DIVIDE STATE_NORTHERN_DIVIDE STATE_TAGTO_PENNINSULA STATE_DJINNAKAH STATE_ARDIMYAN_DESERT STATE_VISAGE_ISLES STATE_CRESCENT_LINE }
}

region_upper_fangaula = {
	capital_province = "x0B5BC5" #random province
	map_color = { 191 186 174 }
	states = { STATE_ZANGALAND STATE_GARPIX_GOXMA STATE_ZIKAAZ STATE_FANGHASBA STATE_JANAMA STATE_JANFANCISO STATE_WARABANI STATE_SOUTHERN_SANDSPITE }
}

region_dao_nako = {
	capital_province = "x0E5E18" #random province
	map_color = { 115 113 109 }
	states = { STATE_GEMTREE STATE_AZZIZA_TU STATE_ASANBOSAM_TU STATE_NEGETU STATE_MARID_DELTA STATE_KUNOLO }
}