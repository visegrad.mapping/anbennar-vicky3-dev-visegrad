﻿pmg_base_building_construction_sector = {
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	ai_selection = most_productive

	production_methods = {
		pm_wooden_buildings
		pm_iron_frame_buildings
		pm_steel_frame_buildings
		pm_arc_welded_buildings
	}
}

pmg_magical_construction = { # Anbennar
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_magical_construction # Anbennar
		pm_fabrication_spells_buildings # Anbennar
		pm_mimic_precursor_steel_buildings # Anbennar
	}
}

pmg_automation_construction = { # Anbennar
	texture = "gfx/interface/icons/generic_icons/mixed_icon_base.dds"
	production_methods = {
		pm_no_automation # Anbennar
		pm_builder_automata # Anbennar
		pm_arithmaton_engineering # Anbennar
		pm_builder_automata_enforced # Anbennar
		pm_arithmaton_engineering_enforced # Anbennar
	}
}