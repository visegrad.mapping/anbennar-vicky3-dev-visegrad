﻿je_torrisheah = {
    icon = "gfx/interface/icons/event_icons/event_protest.dds"

    group = je_group_historical_content

    immediate = {
        trigger_event = { id = anb_torrisheah.001 }
        c:B15 = {
            add_modifier = {
                name = torrisheah_overstretched
            }
        }
    }

    complete = {
        c:B15 = {
            NOT = {
                any_scope_state = {
                    OR = {
                        is_incorporated = no
                        turmoil > 0.2
                    }
                }
            }
            bureaucracy > 0
            weekly_net_fixed_income > 0
        }
    }

    on_complete = {
        trigger_event = { id = anb_torrisheah.002 }
    }

    fail = {
        OR = {
            c:B15 = {
                in_default = yes
            }
            c:B15 = {
                NOT = {
                    owns_entire_state_region = STATE_FALILTCOST
                    owns_entire_state_region = STATE_SANCTUARY_FIELDS
                    owns_entire_state_region = STATE_SILVERTENNAR
                }
            }
        }
    }

    on_fail = {
        c:B14 = {
            set_variable = torrisheah_fail
            trigger_event = { id = anb_torrisheah.003 }
        }
    }

    timeout = 3650

    on_timeout = {
        c:B14 = { 
            set_variable = torrisheah_timeout
            trigger_event = { id = anb_torrisheah.003 }
        }
    }

    on_monthly_pulse = {
        #Random events
        random_events = {
            80 = 0
            5 = anb_torrisheah.004
            5 = anb_torrisheah.008
            5 = anb_torrisheah.012
            5 = anb_torrisheah.016
        }
    }

    should_be_pinned_by_default = yes
}