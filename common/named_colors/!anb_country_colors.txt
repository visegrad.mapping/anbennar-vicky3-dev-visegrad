colors = {	
	#Anbennar
	blackpowder_anbennar = hsv360 { 192 73 6 }
	chippengard_yellow	=	rgb { 209 197 154 }
	tipney_red	=	rgb { 209 130 203 }
	corinsfield_orange	= rgb { 233 122 86 }
	blue_lorent = rgb { 15 117 187 }
	vurebindika_colour = rgb { 180  80  20 }
	irsukumbha_colour = rgb { 100  214  11 }
} 