﻿demilitarized_zone = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_flag_negative.dds
	
	state_building_barracks_max_level_add = -15
	state_building_conscription_center_max_level_add = -15
}

age_of_officers_modifier = {
	icon = gfx/interface/icons/timed_modifier_icons/modifier_statue_positive.dds
	interest_group_ig_armed_forces_pop_attraction_mult = 0.1
	interest_group_ig_armed_forces_pol_str_mult = 0.3
	country_officers_pol_str_mult = 0.5
}
