﻿CHARACTERS = {
	c:A10 ?= {
		create_character = {
			first_name = "Bakram"
			last_name = "Ironsea"
			historical = yes
			age = 33
			culture = cu:grombari_half_orc
			interest_group = ig_armed_forces
			ruler = yes
			ideology = ideology_reformer
			traits = {
				innovative
				wrathful
			}
		}
		
		# The continuation of Ebonfrost dynasty Gerudia deserves
		create_character = {
			first_name = "Bakram"
			last_name = "Ebontusk"
			historical = yes
			age = 37
			culture = cu:grombari_half_orc
			interest_group = ig_industrialists
			ig_leader = yes
			ideology = ideology_royalist
			traits = {
				ambitious
				charismatic
			}
		}
		
		#Leader of Bastani Old Guard
		create_character = {
			first_name = "Urgol"
			last_name = "Doombringer"
			historical = yes
			age = 46
			interest_group = ig_armed_forces
			ig_leader = yes
			ideology = ideology_jingoist_leader
			traits = {
				cruel
				bigoted
			}
		}
		
		#General who defended Garok during last war against Northern League
		create_character = {
			first_name = "Kavu"
			last_name = "Wavebreaker"
			historical = yes
			age = 50
			interest_group = ig_armed_forces
			is_general = yes
			ideology = ideology_moderate
			traits = {
				imposing
				stalwart_defender
				reserved
			}
		}
	}
}
