﻿CHARACTERS = {
	c:B37 ?= {
		create_character = { # longtime merc with ties to ynnic nobility, chosen to lead for his political alignment more than anything. Is worried about the rise of the Ranger Republic, but none have listened to his concerns yet. Is not fond of the separation of church and state that exists in ynngard.
			first_name = Serondar
			last_name =  Windarrow
			historical = yes
			age = 129
			ruler = yes
			interest_group = ig_devout
			ig_leader = yes
			commander = yes
			ideology = ideology_venaanist
			traits = {
				direct romantic stalwart_defender
			}
		}
	}
}
