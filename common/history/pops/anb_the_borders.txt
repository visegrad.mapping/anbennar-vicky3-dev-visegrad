﻿POPS = {

	s:STATE_SUGAMBER = {
		region_state:A02 = {
			create_pop = {
				culture = wexonard
				size = 900610
				split_religion = {
					wexonard = {
						ravelian = 0.25
						corinite = 0.75
					}
				}
			}
			create_pop = {
				culture = arannese
				size = 45023
			}
		}
	}

	s:STATE_BARDSWOOD = {
		region_state:A02 = {
			create_pop = {
				culture = arannese
				size = 650500
			}
			create_pop = {
				culture = wexonard
				size = 584001
				split_religion = {
					wexonard = {
						ravelian = 0.2
						corinite = 0.8
					}
				}
			}
			create_pop = {
				culture = creek_gnome
				size = 34050	#Tellum gnomes
			}
			create_pop = {
				culture = beefoot_halfling
				size = 115030
				split_religion = {
					beefoot_halfling = {
						ravelian = 0.2
						corinite = 0.8
					}
				}
			}
		}
	}

	s:STATE_CELLIANDE = {
		region_state:A02 = {
			create_pop = {
				culture = arannese
				size = 1704570
			}
			create_pop = {
				culture = roilsardi	#gamers
				size = 240836
			}
		}
	}

	s:STATE_TOARNEN = {
		region_state:A02 = {
			create_pop = {
				culture = arannese
				size = 977813
			}
			create_pop = {
				culture = roilsardi	#gamers
				size = 690401
			}
		}
	}

	s:STATE_ASHENIANDE = {
		region_state:A02 = {
			create_pop = {
				culture = arannese
				size = 1896501
			}
			create_pop = {
				culture = esmari
				size = 862401
				split_religion = {
					arannese = {
						ravelian = 0.8
						regent_court = 0.2
					}
				}
			}
			create_pop = {
				culture = havener_elf
				size = 57308
				split_religion = {
					havener_elf = {
						ravelian = 0.3
						elven_forebears = 0.3
						regent_court = 0.4
					}
				}
			}
		}
		region_state:A33 = {
			create_pop = {
				culture = arannese
				size = 160401
			}
		}
	}

	s:STATE_ARANNEN = {
		region_state:A02 = {
			create_pop = {
				culture = arannese
				size = 2710500
			}
			create_pop = {
				culture = beefoot_halfling
				size = 270204
			}
		}
	}

	s:STATE_GISDEN = {
		region_state:A02 = {
			create_pop = {
				culture = arannese
				size = 1084006
			}
		}
	}

	s:STATE_CORVURIAN_PLAINS = {
		region_state:A02 = {
			create_pop = {
				culture = corvurian
				size = 2840010
				split_religion = {
					corvurian = {
						ravelian = 0.4
						corinite = 0.6
					}
				}
			}
		}
	}

	s:STATE_THE_LAND = {
		region_state:A02 = {
			create_pop = {
				culture = corvurian
				size = 3706894
				split_religion = {
					corvurian = {
						ravelian = 0.3
						corinite = 0.7
					}
				}
			}
		}
	}

	s:STATE_BLACKWOODS = {
		region_state:A02 = {
			create_pop = {
				culture = corvurian
				size = 1510879
			}
		}
	}

	
	
	s:STATE_CORVELD ={
		region_state:A02={
			create_pop = {
				culture = arannese
				size = 160500 # Bit higher cause Corveld is here
			}
			create_pop = {
				culture = corvurian
				size = 160900
				split_religion = {
					corvurian = {
						ravelian = 0.2
						corinite = 0.8
					}
				}
			}
			create_pop = {
				culture = flamemarked_gnoll
				size = 22900
			}
		}
		region_state:A70={
			create_pop = {
				culture = nathalairey
				size = 400900 # V big city
			}
		}
		region_state:A76={
			create_pop = {
				culture = flamemarked_gnoll
				size = 30900
			}
		}
	}
	
	s:STATE_FENNFORT ={
		region_state:A02={
			create_pop = {
				culture = arannese
				size = 326500
			}
			create_pop = {
				culture = corvurian
				size = 112500
				split_religion = {
					corvurian = {
						ravelian = 0.2
						corinite = 0.8
					}
				}
			}
			create_pop = {
				culture = flamemarked_gnoll
				size = 12500
			}
		}
		region_state:A76={
			create_pop = {
				culture = flamemarked_gnoll
				size = 31100
			}
		}
	}
	
	s:STATE_DREADMIRE ={
		region_state:A02={
			create_pop = {
				culture = arannese
				size = 56500
			}
			create_pop = {
				culture = corvurian
				size = 91502
				split_religion = {
					corvurian = {
						ravelian = 0.2
						corinite = 0.8
					}
				}
			}
			create_pop = {
				culture = flamemarked_gnoll
				size = 16505
			}
		}
		region_state:A76={
			create_pop = {
				culture = flamemarked_gnoll
				size = 42300
			}
		}
	}
	
	s:STATE_RAVENGARD ={
		region_state:A02={
			create_pop = {
				culture = arannese
				size = 12500
			}
			create_pop = {
				culture = corvurian
				size = 645100
				split_religion = {
					corvurian = {
						ravelian = 0.2
						corinite = 0.8
					}
				}
			}
			create_pop = {
				culture = flamemarked_gnoll
				size = 16505
			}
		}
		region_state:A76={
			create_pop = {
				culture = flamemarked_gnoll
				size = 52300
			}
		}
	}
	s:STATE_OURDIA ={
		region_state:A80={
			create_pop = {
				culture = ourdi
				size = 1159400
			}
			create_pop = {
				culture = soyzkaru_goblin
				size = 710600
			}
		}
	}
	s:STATE_HONDERAAK = {
		region_state:A11 = {
			create_pop = {
				culture = dovesworn_gnoll
				size = 505930
			}
		}
	}
}