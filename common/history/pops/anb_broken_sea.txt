﻿POPS = {
	s:STATE_TRITHEMAR = {
		region_state:B59 = {
			create_pop = {
				culture = themarian
				size = 387200
			}
			create_pop = {
				culture = vrendzani_kobold
				size = 21400
				split_religion = {
					vrendzani_kobold = {
						the_thought = 0.8
						kobold_dragon_cult = 0.2
					}
				}
			}
			create_pop = {
				culture = cliff_gnome
				size = 5400
			}
			create_pop = {
				culture = cursed_one
				size = 15400
			}
		}
	}
	s:STATE_SILDARBAD = {
		region_state:B62 = {
			create_pop = {
				culture = vrendzani_kobold
				size = 20600
				split_religion = {
					vrendzani_kobold = {
						the_thought = 0.8
						kobold_dragon_cult = 0.2
					}
				}
			}
			create_pop = {
				culture = cliff_gnome
				size = 1300
			}
			create_pop = {
				culture = themarian
				size = 2600
			}
			create_pop = {
				culture = reverian
				size = 1400
			}
		}
		region_state:B51 = {
			create_pop = {
				culture = cursed_one
				size = 75000
			}
		}
		region_state:G08 = {
			create_pop = {
				culture = cliff_gnome
				size = 1700
			}
			create_pop = {
				culture = themarian
				size = 8950
			}
			create_pop = {
				culture = reverian
				size = 7000
			}
		}
	}
	s:STATE_FARNOR = {
		region_state:B62 = {
			create_pop = {
				culture = vrendzani_kobold
				size = 53480
				split_religion = {
					vrendzani_kobold = {
						the_thought = 0.8
						kobold_dragon_cult = 0.2
					}
				}
			}
			create_pop = {
				culture = cliff_gnome
				size = 2520
			}
			create_pop = {
				culture = rzentur
				religion = vechnogejzn
				size = 21540
			}
		}
		region_state:B50 = {
			create_pop = {
				culture = rzentur
				religion = vechnogejzn
				size = 12500
			}
		}
	}
	s:STATE_DALAIRRSILD = {
		region_state:G08 = {
			create_pop = {
				culture = vrendzani_kobold
				size = 18680
				split_religion = {
					vrendzani_kobold = {
						the_thought = 0.85
						kobold_dragon_cult = 0.15
					}
				}
			}
			create_pop = {
				culture = dalairey
				size = 25350
			}
			create_pop = {
				culture = cliff_gnome
				size = 1600
			}
			create_pop = {
				culture = teira
				size = 15910
			}
			create_pop = {
				culture = cursed_one
				size = 2010
			}
		}
		region_state:G06 = {
			create_pop = {
				culture = vrendzani_kobold
				size = 89200
			}
			create_pop = {
				culture = dalairey
				size = 125400
			}
			create_pop = {
				culture = cliff_gnome
				size = 4800
			}
			create_pop = {
				culture = cursed_one
				size = 10000
			}
			create_pop = {
				culture = teira
				size = 40600
			}
		}
		region_state:G09 = {
			create_pop = {
				culture = teira
				size = 54520
			}
		}
	}
	s:STATE_HJORDAL = {
		region_state:B61 = {
			create_pop = {
				culture = dalairey
				size = 269780
			}
			create_pop = {
				culture = vrendzani_kobold
				size = 237600
				split_religion = {
					vrendzani_kobold = {
						the_thought = 0.95
						kobold_dragon_cult = 0.05
					}
				}
			}
			create_pop = {
				culture = cliff_gnome
				size = 29050
			}
			create_pop = {
				culture = teira
				size = 28590
			}
			create_pop = {
				culture = freemarcher_half_orc
				size = 17500
			}
		}
		region_state:G05 = {
			create_pop = {
				culture = vrendzani_kobold
				size = 188020
				split_religion = {
					vrendzani_kobold = {
						the_thought = 0.95
						kobold_dragon_cult = 0.05
					}
				}
			}
			create_pop = {
				culture = dalairey
				size = 165380
			}
			create_pop = {
				culture = cliff_gnome
				size = 1450
			}
			create_pop = {
				culture = teira
				size = 9090
			}
			create_pop = {
				culture = freemarcher_half_orc
				size = 8060
			}
		}
		region_state:G06 = {
			create_pop = {
				culture = dalairey
				size = 214520
			}
			create_pop = {
				culture = vrendzani_kobold
				size = 142660
				split_religion = {
					vrendzani_kobold = {
						the_thought = 0.9
						kobold_dragon_cult = 0.1
					}
				}
			}
			create_pop = {
				culture = cliff_gnome
				size = 7680
			}
			create_pop = {
				culture = teira
				size = 24220
			}
			create_pop = {
				culture = freemarcher_half_orc
				size = 6920
			}
		}
	}
	s:STATE_FLOTTNORD = {
		region_state:G06 = {
			create_pop = {
				culture = dalairey
				size = 42100
			}
			create_pop = {
				culture = vrendzani_kobold
				size = 31540
				split_religion = {
					vrendzani_kobold = {
						the_thought = 0.9
						kobold_dragon_cult = 0.1
					}
				}
			}
			create_pop = {
				culture = cliff_gnome
				size = 5180
			}
			create_pop = {
				culture = teira
				size = 1020
			}
			create_pop = {
				culture = freemarcher_half_orc
				size = 4080
			}
		}
		region_state:A04 = {
			create_pop = {
				culture = dalairey
				size = 20060
			}
			create_pop = {
				culture = gawedi
				size = 17000
			}
			create_pop = {
				culture = teira
				size = 11020
			}
		}
	}
	s:STATE_SJAVARRUST = {
		region_state:G07 = {
			create_pop = {
				culture = vrendzani_kobold
				size = 6560
				split_religion = {
					vrendzani_kobold = {
						the_thought = 0.8
						kobold_dragon_cult = 0.2
					}
				}
			}
			create_pop = {
				culture = dalairey
				size = 19100
			}
		}
		region_state:G08 = {
			create_pop = {
				culture = vrendzani_kobold
				size = 19920
				split_religion = {
					vrendzani_kobold = {
						the_thought = 0.8
						kobold_dragon_cult = 0.2
					}
				}
			}
			create_pop = {
				culture = dalairey
				size = 31970
			}
			create_pop = {
				culture = cliff_gnome
				size = 2810
			}
			create_pop = {
				culture = teira
				size = 22640
			}
		}
		region_state:B60 = {
			create_pop = {
				culture = glacier_gnome
				size = 5200
			}
		}
		region_state:B59 = {
			create_pop = {
				culture = themarian
				size = 125980
			}
		}
	}
	s:STATE_NYHOFN = {
		region_state:G07 = {
			create_pop = {
				culture = dalairey
				size = 28296
			}
			create_pop = {
				culture = vrendzani_kobold
				size = 5936
				split_religion = {
					vrendzani_kobold = {
						the_thought = 0.8
						kobold_dragon_cult = 0.2
					}
				}
			}
			create_pop = {
				culture = cliff_gnome
				size = 2728
			}
		}
	}
	s:STATE_DALAIREY_WASTES = {
		region_state:A06 = {
			create_pop = {
				culture = cliff_gnome
				size = 2280
			}
			create_pop = {
				culture = reverian
				size = 3720
			}
		}
		region_state:B17 = {
			create_pop = {
				culture = star_elf
				size = 39600
			}
		}
		region_state:B64 = {
			create_pop = {
				culture = cursed_one
				size = 5400
			}
		}
	}
	s:STATE_FIORGAM = {
		region_state:A04 = {
			create_pop = {
				culture = derannic
				size = 103000
			}
			create_pop = {
				culture = gawedi
				size = 93000
			}
		}
		region_state:A03 = {
			create_pop = {
				culture = lorentish
				size = 78000
			}
		}
	}
	s:STATE_MITTANWEK = {
		region_state:B60 = {
			create_pop = {
				culture = glacier_gnome
				size = 41050
			}
		}
	}
	s:STATE_FARPLOTT = {
		region_state:B60 = {
			create_pop = {
				culture = glacier_gnome
				size = 16810
			}
			create_pop = {
				culture = snecboth
				size = 8190
			}
		}
	}
}