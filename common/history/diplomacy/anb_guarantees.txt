﻿DIPLOMACY = {
	c:B49 = { #Bosancovac is the Ynn's Novigrad and both DD and Sarda wants to stop the other from getting it
		create_diplomatic_pact = {
			country = c:B39
			type = guarantee_independence
		}	
	}
	c:B29 = {
		create_diplomatic_pact = {
			country = c:B39
			type = guarantee_independence
		}	
	}
	c:A09 = {
		create_diplomatic_pact = {
			country = c:F02
			type = guarantee_independence
		}	
	}

	c:L34 = { #Adzalan
		create_diplomatic_pact = {
			country = c:L31 #Thunder Wielding Buffalo
			type = guarantee_independence
		}	
	}
}
