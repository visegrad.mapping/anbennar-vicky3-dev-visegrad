﻿COUNTRIES = {
	c:A20 ?= {
		effect_starting_technology_tier_3_tech = yes
		add_technology_researched = tradition_of_equality
		
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_racial_segregation	#they should accept farrani etc so whatever does that
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_appointed_bureaucrats	#as per elven long life and ship stuff, best pick the best person, elected not allowed without voting
		activate_law = law_type:law_professional_army
		
		activate_law = law_type:law_traditionalism
		activate_law = law_type:law_mercantilism	#exporting elven goods to Cannor
		activate_law = law_type:law_land_based_taxation	#elven long life prob taught to tax all not just landowner. BUT Per-capita not allowed under traditionalism!
		activate_law = law_type:law_tenant_farmers
		
		activate_law = law_type:law_censorship
		#activate_law = law_type:law_serfdom_banned
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_same_heritage_only
		
		
		activate_law = law_type:law_artifice_banned



	}
}