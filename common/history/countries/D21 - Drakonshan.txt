﻿COUNTRIES = {
	c:D21 ?= {
		effect_starting_technology_tier_4_tech = yes
		add_technology_researched = cotton_gin
		add_technology_researched = lathe
		add_technology_researched = mandatory_service
		add_technology_researched = line_infantry
		add_technology_researched = urban_planning
		add_technology_researched = law_enforcement
		add_technology_researched = colonization
		add_technology_researched = academia
		add_technology_researched = mass_communication
		add_technology_researched = medical_degrees
		
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_autocracy
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_hereditary_bureaucrats
		activate_law = law_type:law_peasant_levies
		# No home affairs

		activate_law = law_type:law_traditionalism
		activate_law = law_type:law_isolationism
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_colonial_exploitation
		activate_law = law_type:law_local_police
		# No schools
		# No health system
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_serfdom
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_no_social_security
		activate_law = law_type:law_closed_borders
		activate_law = law_type:law_slave_trade

		activate_law = law_type:law_same_heritage_only
		
		
		activate_law = law_type:law_nation_of_magic

		#add_journal_entry = { type = je_goblin_tide_drak }
	}
}