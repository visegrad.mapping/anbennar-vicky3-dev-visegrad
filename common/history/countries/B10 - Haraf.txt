﻿COUNTRIES = {
	c:B10 ?= {
		effect_starting_technology_tier_5_tech = yes #in-between T5 and T4
		add_technology_researched = prospecting
		add_technology_researched = shaft_mining
		add_technology_researched = gunsmithing
		add_technology_researched = centralization
		add_technology_researched = democracy
		add_technology_researched = international_trade
		
		activate_law = law_type:law_presidential_republic
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_hereditary_bureaucrats
		activate_law = law_type:law_professional_army
		# No home affairs

		activate_law = law_type:law_traditionalism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_land_based_taxation
		# No colonial affairs
		# No police
		# No school system
		activate_law = law_type:law_no_health_system
		
		activate_law = law_type:law_right_of_assembly
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_no_social_security
		# No migration controls
		activate_law = law_type:law_debt_slavery

		activate_law = law_type:law_expanded_tolerance
		
		
		activate_law = law_type:law_nation_of_artifice

		ig:ig_landowners = {
			add_ruling_interest_group = yes
		}
		ig:ig_armed_forces = {
			add_ruling_interest_group = yes
		}
	}
}