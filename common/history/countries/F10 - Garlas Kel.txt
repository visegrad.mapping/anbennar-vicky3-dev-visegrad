﻿COUNTRIES = {
	c:F10 ?= {
		effect_starting_technology_tier_4_tech = yes
		add_technology_researched = tradition_of_equality
		
		activate_law = law_type:law_magocracy
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_national_militia
		
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_tenant_farmers
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_migration_controls
		
		activate_law = law_type:law_same_heritage_only
		
		
		activate_law = law_type:law_nation_of_magic
	}
}