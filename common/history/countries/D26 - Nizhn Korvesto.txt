﻿COUNTRIES = {
	c:D26 ?= {
		effect_starting_technology_tier_4_tech = yes
		add_technology_researched = cotton_gin
		add_technology_researched = lathe
		add_technology_researched = mechanical_tools	#kanzad
		add_technology_researched = mandatory_service
		add_technology_researched = line_infantry
		add_technology_researched = napoleonic_warfare	#kanzad
		add_technology_researched = urban_planning
		add_technology_researched = law_enforcement
		add_technology_researched = colonization
		add_technology_researched = academia
		
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_autocracy
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_freedom_of_conscience
		activate_law = law_type:law_hereditary_bureaucrats
		activate_law = law_type:law_peasant_levies
		# No home affairs

		activate_law = law_type:law_traditionalism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_colonial_resettlement #revert to frontier when pdx fixes impassable provinces bug
		activate_law = law_type:law_local_police
		# No schools
		# No health system
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_serfdom
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_no_womens_rights
		activate_law = law_type:law_no_social_security
		activate_law = law_type:law_no_migration_controls
		activate_law = law_type:law_debt_slavery

		activate_law = law_type:law_local_tolerance
		
		
		activate_law = law_type:law_nation_of_magic

		set_institution_investment_level = {
			institution = institution_police
			level = 2
		}
	}
}