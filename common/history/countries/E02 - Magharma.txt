﻿COUNTRIES = {
	c:E02 ?= {
		effect_starting_technology_tier_3_tech = yes

		add_technology_researched = centralization
		add_technology_researched = cotton_gin
		
		effect_starting_politics_traditional = yes
		activate_law = law_type:law_oligarchy
		activate_law = law_type:law_professional_army
		activate_law = law_type:law_interventionism
		activate_law = law_type:law_mercantilism
		activate_law = law_type:law_land_based_taxation
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_local_police
		activate_law = law_type:law_charitable_health_system

		activate_law = law_type:law_debt_slavery
		
		activate_law = law_type:law_local_tolerance
		
		
		activate_law = law_type:law_traditional_magic_banned
		
		activate_law = law_type:law_women_own_property
		activate_law = law_type:law_migration_controls
	}
}