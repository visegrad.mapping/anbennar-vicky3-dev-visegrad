﻿COUNTRIES = {
	c:B22 ?= {
		effect_starting_technology_tier_2_tech = yes

		add_technology_researched = egalitarianism

		effect_starting_politics_liberal = yes
		
		activate_law = law_type:law_monarchy
		activate_law = law_type:law_census_voting #The akal has no real power
		activate_law = law_type:law_national_supremacy
		activate_law = law_type:law_state_religion
		activate_law = law_type:law_appointed_bureaucrats
		activate_law = law_type:law_national_militia
		# No home affairs
		activate_law = law_type:law_interventionism
		activate_law = law_type:law_per_capita_based_taxation
		# No colonial affairs
		# No police
		activate_law = law_type:law_religious_schools
		activate_law = law_type:law_charitable_health_system
		activate_law = law_type:law_artifice_banned #Pro-mage
		
		activate_law = law_type:law_censorship
		activate_law = law_type:law_tenant_farmers
		activate_law = law_type:law_child_labor_allowed
		activate_law = law_type:law_women_own_property
		# No social security
		activate_law = law_type:law_migration_controls #Still somewhat isolationist
		activate_law = law_type:law_slavery_banned
		
		activate_law = law_type:law_local_tolerance

		ig:ig_landowners = {
			set_interest_group_name = ig_planter_gentry
		}
	}
}