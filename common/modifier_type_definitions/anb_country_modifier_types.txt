﻿country_magical_expertise_add = {
	color = good
	percent = no
	decimals = 0
	game_data={
		ai_value=40
	}
}

country_magical_expertise_mult = {
	color = good
	percent = yes
	decimals = 0
}

country_magical_expertise_cost_per_institution_mult = {
	color = good
	percent = yes
	decimals = 0
}

country_magical_expertise_decree_cost = {
	color = bad
	percent = no
	decimals = 0
	game_data={
		ai_value=1
	}
}

country_magical_expertise_decree_cost_mult = {
	color = bad
	percent = yes
	decimals = 0
}

country_bad_weather_chance_mult = {
	color = bad
	percent = yes
	decimals = 0
}