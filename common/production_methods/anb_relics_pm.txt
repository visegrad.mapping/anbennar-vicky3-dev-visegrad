pm_arcane_antiquarians_building_relics = {
	texture = "gfx/interface/icons/production_method_icons/picks_and_shovels.dds"

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_magical_reagents_add = 5
			goods_input_tools_add = 5
			
			# output goods
			goods_output_curiosity_add = 12
			goods_output_flawless_metal_add = 12
		}

		level_scaled = {
			building_employment_mages_add = 1000
			building_employment_laborers_add = 3500
			building_employment_shopkeepers_add = 500
		}
	}
}

pm_remote_sensing_surveys_building_relics = {
	texture = "gfx/interface/icons/production_method_icons/sweeteners.dds"
	
	unlocking_technologies = {
		
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_magical_reagents_add = 15
			goods_input_tools_add = 10
			goods_input_sulfur_add = 5
			
			# output goods
			goods_output_curiosity_add = 24
			goods_output_flawless_metal_add = 24
		}

		level_scaled = {
			building_employment_academics_add = 500
			building_employment_mages_add = 500
			building_employment_laborers_add = 3500
			building_employment_shopkeepers_add = 500
		}
	}
}

pm_psychometric_excavators_building_relics = {
	texture = "gfx/interface/icons/production_method_icons/furniture_handicraft.dds"
	
	unlocking_technologies = {
		
	}

	building_modifiers = {
		workforce_scaled = {
			# input goods
			goods_input_magical_reagents_add = 25
			goods_input_tools_add = 15
			goods_input_sulfur_add = 10
			goods_input_artificery_doodads_add = 5
			
			# output goods
			goods_output_curiosity_add = 40
			goods_output_flawless_metal_add = 40
		}

		level_scaled = {
			building_employment_academics_add = 1000
			building_employment_laborers_add = 3500
			building_employment_shopkeepers_add = 500
		}
	}
}