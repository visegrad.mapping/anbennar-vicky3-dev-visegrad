﻿building_prikoyol_canal = {
	building_group = bg_canals
	icon = "gfx/interface/icons/building_icons/panama_canal.dds"
	canal = canal_prikoyol
	expandable = no
	downsizeable = no
	unique = yes
	required_construction = construction_cost_canal
	
	entity_not_constructed = { }
	entity_under_construction = { "canal_panamacanal_under_construction_01_entity" }
	entity_constructed = { "canal_panamacanal_01_entity" }
	locator = "canal_panama_locator"

	city_gfx_interactions = {
		clear_collision_mesh_area = yes
		clear_size_area = no
	}

	potential = {
		state_region = s:STATE_PRIKOYOL
	}

	possible = {
		owner = {
			OR = {
				owns_treaty_port_in = STATE_PRIKOYOL
				owns_entire_state_region = STATE_PRIKOYOL
			}
		}
	}
	production_method_groups = {
		pmg_canal
	}
}
#
building_dhal_nikhuvad_canal = {
	building_group = bg_canals
	icon = "gfx/interface/icons/building_icons/suez_canal.dds"
	canal = canal_dhal_nikhuvad
	expandable = no
	downsizeable = no
	unique = yes
	
	required_construction = construction_cost_canal
	

	entity_not_constructed = { }
	entity_under_construction = { "canal_suezcanal_under_construction_01_entity" }
	entity_constructed = { "canal_suezcanal_01_entity" }
	locator = "canal_suez_locator"

	city_gfx_interactions = {
		clear_collision_mesh_area = yes
		clear_size_area = no
	}
	

potential = {
	state_region = s:STATE_BEERAGGA
}

possible = {
	owner = {
		OR = {
			owns_treaty_port_in = STATE_BEERAGGA
			owns_entire_state_region = STATE_BEERAGGA
		}
		custom_tooltip = {
			text = dhal_nikhuvad_survey_complete_tooltip
			has_variable = dhal_nikhuvad_survey_complete
		}
	}
}
	production_method_groups = {
		pmg_canal
	}
}