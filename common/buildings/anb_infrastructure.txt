﻿building_arcane_nexus = {
	building_group = bg_arcane_nexus
	icon = "gfx/interface/icons/building_icons/arcane_nexus.dds"
	background = "gfx/interface/icons/building_icons/backgrounds/building_panel_bg_monuments.dds"
	city_type = city
	levels_per_mesh = 0
	ownership_type = self
	
	production_method_groups = {
		pmg_base_building_arcane_nexus
		pmg_transmuation_building_arcane_nexus
	}
	
	required_construction = construction_cost_low
}